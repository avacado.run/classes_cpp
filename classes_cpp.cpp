#include <iostream>

using namespace std;

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        cout << x << ends << y << ends << z << endl;
    }
    float vectorLenght()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }
    
private:
    double x, y, z;
};

class Dog
{
private:
    string name;
    string breed;
    int age;
public:
    Dog() : name("Jack"), breed("Labrador"), age(5)
    {}
    Dog(string _name, string _breed, int _age) : name(_name), breed(_breed), age(_age)
    {}
    void whatADog()
    {
        cout << "Dog named:" << ends << name << endl;
        cout << "Breed: " << ends << breed << endl;
        cout << "Age: " << ends << age << endl;
    }
};

int main(int argc, char* argv[])
{
    Vector v(10, 20, 30);
    v.Show();
    cout << "Vector Lenght:" << ends << v.vectorLenght() << endl;
    Dog dog("Rufus", "poodle", 3);
    dog.whatADog();
    return 0;
}
